For instructions on how to compile and use this software, translate the LaTeX documentation that you can find in the "Documentation" directory with the commands

cd Documentation
make handbook

and read the first section of the resulting PDF file called 'ART_Handbook.pdf'. Alternatively, you should be able to find an already translated version of this file on the ART homepage where you obtained this source tree.